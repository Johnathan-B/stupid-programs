#include <stdio.h>
#include <float.h>
#include <math.h>
#include <stdlib.h>
#include "sphere.h"
#include "hitable_list.h"
#include "camera.h"
#include "helper.h"


int main()
{
	int i, j, s, nx, ny, ns;
	int ir, ig, ib;
	float u, v;
	hitable *list[2];
	hitable *world;
	vec3 p;
	camera cam;

	nx = 200;
	ny = 100;
	ns = 100;

	// Print the P3 file header
	printf("P3\n%d %d\n255\n", nx, ny);

	list[0] = new sphere(vec3(0.0, 0.0, -1.0), 0.5);
	list[1] = new sphere(vec3(0.0, -100.5, -1.0), 100);

	world = new hitable_list(list, 2);

	for(j = ny - 1; j >= 0; j--)
	{
		for(i = 0; i < nx; i++)
		{
			vec3 col(0.0, 0.0, 0.0);
			for(s = 0; s < ns; s++)
			{
				u = (float)(i + drand48()) / (float)nx;
				v = (float)(j + drand48()) / (float)ny;
				ray r = cam.get_ray(u, v);
				p = r.point_at_parameter(2.0);
				col += color(r, world);
			}

			col /= (float)ns;

			ir = (int)(255.99*col[0]);
			ig = (int)(255.99*col[1]);
			ib = (int)(255.99*col[2]);

			// Print the current pixel's color values
			printf("%d %d %d\n", ir, ig, ib);
		}
	}
}
