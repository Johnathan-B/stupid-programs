#ifndef SPHEREH
#define SPHEREH
#include "hitable.h"

class sphere: public hitable
{
	public:
		sphere() {}
		sphere(vec3 cen, float r) : center(cen), radius(r) {};
		virtual bool hit(const ray& r, float t_min, float t_max, hit_record& rec) const;

		vec3 center;
		float radius;
};

bool sphere::hit(const ray& r, float t_min, float t_max, hit_record& rec) const
{
	float a, b, c, discriminant, tmp;
	vec3 oc;

	oc = r.origin() - center;
	a = dot(r.direction(), r.direction());
	b = dot(oc, r.direction());
	c = dot(oc, oc) - radius * radius;
	discriminant = b * b - a * c;

	if(discriminant > 0)
	{
		tmp = (-b - sqrt(discriminant)) / a;

		if(tmp < t_max && tmp > t_min)
		{
			rec.t = tmp;
			rec.p = r.point_at_parameter(rec.t);
			rec.normal = (rec.p - center) / radius;
			return true;
		}

		tmp = (-b + sqrt(discriminant)) / a;

		if(tmp < t_max && tmp > t_min)
		{
			rec.t = tmp;
			rec.p = r.point_at_parameter(rec.t);
			rec.normal = (rec.p - center) / radius;
			return true;
		}
	}
	return false;
}
#endif
