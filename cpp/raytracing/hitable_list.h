#ifndef HITABLELISTH
#define HITABLELISTH
#include "hitable.h"

class hitable_list: public hitable
{
	public:
		hitable_list() {}
		hitable_list(hitable **l, int n);

		virtual bool hit(const ray& r, float t_min, float t_max, hit_record& rec) const;
		hitable **list;
		int list_size;
};

hitable_list::hitable_list(hitable **l, int n)
{
	list = l;
	list_size = n;
}

bool hitable_list::hit(const ray& r, float t_min, float t_max, hit_record& rec) const
{
	int i;
	bool hit_anything;
	double closest;
	hit_record tmp_rec;

	hit_anything = false;
	closest = t_max;

	for(i = 0; i < list_size; i++)
	{
		if(list[i]->hit(r, t_min, closest, tmp_rec))
		{
			hit_anything = true;
			rec = tmp_rec;
			closest = rec.t;
		}
	}

	return hit_anything;
}

#endif
