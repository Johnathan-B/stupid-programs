#include <stdio.h>
#include <stdlib.h>

int q(int i, int j){printf("%d %d",i,j);for(int k=i;i<j?++k<j:--k>j;)printf(" %d",k);}

int main(int argc, char** argv)
{
	if(argc != 3)
	{
		fprintf(stderr, "Usage: funk n m\n");
		exit(-1);
	}

	q( atoi(argv[1]), atoi(argv[2]) );
	return 0;
}
